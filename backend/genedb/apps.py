from django.apps import AppConfig


class GenedbConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'genedb'
