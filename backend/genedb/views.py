from django.shortcuts import render

from django.http import JsonResponse
from rest_framework.parsers import JSONParser 
from rest_framework import status
 
# from .models import PatientDataModel
# from .api.serializers import PatientDataModelSerializer
from rest_framework.decorators import api_view
from django.db import connection, transaction
from rest_framework.response import Response

# importing networkx
import networkx as nx

from django.http import HttpResponse
import json
import pandas as pd
import math
def dictfetchall(cursor):
    "Returns all rows from a cursor as a dict"
    desc = cursor.description
    return [
        dict(zip([col[0] for col in desc], row))
        for row in cursor.fetchall()
    ]
def percent(x,y):
    #########print(x,y)
    s= x+y
    if s!=0:
        return [round((x/s)*100,2),round((y/s)*100,2)]
    else:
        return [0,0]

def percentall(x):
    #########print(x,y)
    s= sum(x)
    temp = []
    if s!=0:
        for i in x:
            temp.append(round((i/s)*100,2))
        return temp
    else:
        for i in x:
            temp.append(0)
        return temp

def dictfetchall(cursor):
    "Returns all rows from a cursor as a dict"
    desc = cursor.description
    return [
        dict(zip([col[0] for col in desc], row))
        for row in cursor.fetchall()
    ]

def getData(query, cursor):
    cursor.execute(query)
    data = cursor.fetchall()

    colnames = [desc[0] for desc in cursor.description] 
    return colnames, data
def getPd(query, cursor):
    colnames, data = getData(query,cursor)
    df = pd.DataFrame(data, columns=colnames)
    return df, colnames


@api_view(['GET', 'POST', 'DELETE'])
def esanjeevni_data(request):
    cursor = connection.cursor()
    final_data = []
    print("Inside ensanjeevni")
    if request.method == 'POST':

        # patdata = PatientDataModel.objects.all()[:5]
        
        #title = request.GET.get('subcenter_id', None)
        # fromdate=request.POST.get('fromdate')
        # todate=request.POST.get('todate')

        inputs =JSONParser().parse(request)
        # data = request.body.decode('utf-8') 
        # inputs = json.loads(data)


     
        
        print('data',inputs)
        

        final_data = []


        if inputs['status']=='pubmed_search':
            final_data=dict()
            # query1 = """SELECT array_to_json(array_agg(row_to_json(r))) FROM (SELECT * FROM public.genemaster_table_maize where lower(v4_gene_model_id) ilike lower('""" + str(inputs['content'])+ """') )r"""
            table_name ='arabidopsis_evidencemasterfile'
            if inputs['type']=='Maize':
            
                query = """SELECT array_to_json(array_agg(row_to_json(r))) FROM (SELECT * FROM public.evidence_table_maize where lower(pubmedid) ilike '""" + inputs['pubmedid']+"""')r"""
            else:
                query = """SELECT array_to_json(array_agg(row_to_json(r))) FROM (SELECT * FROM public.arabidopsis_evidencemasterfile where lower(pubmedid) ilike '""" + inputs['pubmedid']+"""')r"""

            
            
            # query2 = """select string_agg(t1.seq,E'\n') from (
			# 		SELECT concat(E'> Len:',LENGTH(seq),E'\n',description,E'\n',seq) as "seq" FROM public.zea_mays_b73_refgen_v4_cdna_all_fa
            #         where id ilike '%""" + str(inputs['content'])+ """%' ) t1"""
            data = getData(query,cursor)[1]
                      
            d1={}
            if data:
                if data[0][0]!=None:
                   
                    d1 = data[0][0]

            # print(d1)
            return JsonResponse({'data': d1,'status':'Success'})




        if inputs['status']=='search_cds':
            final_data = dict()
            
            # query1 = """SELECT array_to_json(array_agg(row_to_json(r))) FROM (SELECT * FROM public.genemaster_table_maize where lower(v4_gene_model_id) ilike lower('""" + str(inputs['content'])+ """') )r"""
            
            if inputs['type']=='Maize':
                table_name = 'zea_mays_b73_refgen_v4_cds_all_fa'
            
                query1 = """select string_agg(t1.seq,E'\n') from (
					SELECT concat(E'> Len:',LENGTH(seq),E'\n',description,E'\n',seq) as "seq" FROM public."""+ table_name+""" where id ilike '%""" + str(inputs['content'])+ """%' ) t1"""

            if inputs['type']=='Arabidopsis':
                table_name ='arabidopsis_thaliana_tair10_cds_all_fa'
            
                query1 = """select string_agg(t1.seq,E'\n') from (
					SELECT concat(E'> Len:',LENGTH(seq),E'\n',description,E'\n',seq) as "seq" FROM public."""+ table_name+""" where id ilike '%""" + str(inputs['content'])+ """%' ) t1"""

            
            # query2 = """select string_agg(t1.seq,E'\n') from (
			# 		SELECT concat(E'> Len:',LENGTH(seq),E'\n',description,E'\n',seq) as "seq" FROM public.zea_mays_b73_refgen_v4_cdna_all_fa
            #         where id ilike '%""" + str(inputs['content'])+ """%' ) t1"""
            data = getData(query1,cursor)[1]
            # data2 = getData(query2,cursor)[1]

          
            d1 = {}
            d2 = {}
            if data:
                if data[0][0]!=None:
                   
                    d1 = data[0][0]

            # ##print(data2[0][0])
            # if data2:
            #     if data2[0][0]!=None:
                  
            #         d2 = data2[0][0]

            
            # #print(d1)
            
            return Response({'cdsdata': d1,'status':'Success'})


        if inputs['status']=='search_protein':
            final_data = dict()

            
            if inputs['type']=='Maize':
                table_name1 = 'agpv4_5_uniprot'
                table_name2 = 'zea_mays_b73_refgen_v4_pep_all_fa'
            # query1 = """SELECT array_to_json(array_agg(row_to_json(r))) FROM (SELECT * FROM public.genemaster_table_maize where lower(v4_gene_model_id) ilike lower('""" + str(inputs['content'])+ """') )r"""

                query1 = """SELECT array_to_json(array_agg(row_to_json(r))) FROM (SELECT * FROM public."""+table_name1+""" where gene_stable_id ilike  lower('""" + str(inputs['content'])+ """') )r"""
                query2 = """select string_agg(t1.seq,E'\n') from (
					SELECT concat(E'> Len:',LENGTH(seq),E'\n',description,E'\n',seq) as "seq" FROM public."""+ table_name2+""" where id ilike '%""" + str(inputs['content'])+ """%' ) t1"""
            
            
            
            if inputs['type']=='Arabidopsis':
                table_name1 ='arabidopsis_uniprot'
                table_name2 ='arabidopsis_thaliana_tair10_pep_all_fa'

                query1 = """SELECT array_to_json(array_agg(row_to_json(r))) FROM (SELECT * FROM public."""+table_name1+""" where gene_stable_id ilike  lower('""" + str(inputs['content'])+ """') )r"""
                query2 = """select string_agg(t1.seq,E'\n') from (
					SELECT concat(E'> Len:',LENGTH(seq),E'\n',description,E'\n',seq) as "seq" FROM public."""+ table_name2+""" where id ilike '%""" + str(inputs['content'])+ """%' ) t1"""
            
            
                

            
            data = getData(query1,cursor)[1]
            data2 = getData(query2,cursor)[1]

          
            d1 = {}
            d2 = {}
            if data:
                if data[0][0]!=None:
                   
                    d1 = data[0][0]

            # ##print(data2[0][0])
            if data2:
                if data2[0][0]!=None:
                  
                    d2 = data2[0][0]

            

            
            return Response({'uniprotdata': d1,'protein':d2,'status':'Success'})

        if inputs['status']=='search_ontology':
            final_data = dict()
            
            # query1 = """SELECT array_to_json(array_agg(row_to_json(r))) FROM (SELECT * FROM public.genemaster_table_maize where lower(v4_gene_model_id) ilike lower('""" + str(inputs['content'])+ """') )r"""

            if inputs['type']=='Maize':
                table_name = 'agpv4_5_goterm'

                query1 = """SELECT array_to_json(array_agg(row_to_json(r))) FROM (SELECT * FROM public."""+table_name+""" where lower(geneid) ilike lower('""" + str(inputs['content'])+ """') )r"""
                
            
            if inputs['type']=='Arabidopsis':
                table_name ='arabidopsis_goterm_masterfile'
                query1 = """SELECT array_to_json(array_agg(row_to_json(r))) FROM (SELECT * FROM public."""+table_name+""" where lower(locus) ilike lower('""" + str(inputs['content'])+ """') )r"""
            
            
            # query2 = """select string_agg(t1.seq,E'\n') from (
			# 		SELECT concat(E'> Len:',LENGTH(seq),E'\n',description,E'\n',seq) as "seq" FROM public.zea_mays_b73_refgen_v4_cdna_all_fa
            #         where id ilike '%""" + str(inputs['content'])+ """%' ) t1"""
            data = getData(query1,cursor)[1]
            # data2 = getData(query2,cursor)[1]

          
            d1 = {}
            d2 = {}
            if data:
                if data[0][0]!=None:
                   
                    d1 = data[0][0]

            # ##print(data2[0][0])
            # if data2:
            #     if data2[0][0]!=None:
                  
            #         d2 = data2[0][0]

            
            # #print(d1)
            
            return Response({'ontology': d1,'status':'Success'})

        if inputs['status']=='search_domain':
            final_data = dict()
            
            # query1 = """SELECT array_to_json(array_agg(row_to_json(r))) FROM (SELECT * FROM public.genemaster_table_maize where lower(v4_gene_model_id) ilike lower('""" + str(inputs['content'])+ """') )r"""

            
            if inputs['type']=='Maize':
                table_name = 'agpv4_5_pfam'

                query1 = """SELECT array_to_json(array_agg(row_to_json(r))) FROM (SELECT * FROM public."""+table_name+""" where lower(geneid) ilike lower('""" + str(inputs['content'])+ """') )r"""

            if inputs['type']=='Arabidopsis':
                table_name ='arabidopsis_pfam_masterfile'

                query1 = """SELECT array_to_json(array_agg(row_to_json(r))) FROM (SELECT * FROM public."""+table_name+""" where lower(geneid) ilike lower('""" + str(inputs['content'])+ """') )r"""

            
            # query2 = """select string_agg(t1.seq,E'\n') from (
			# 		SELECT concat(E'> Len:',LENGTH(seq),E'\n',description,E'\n',seq) as "seq" FROM public.zea_mays_b73_refgen_v4_cdna_all_fa
            #         where id ilike '%""" + str(inputs['content'])+ """%' ) t1"""
            data = getData(query1,cursor)[1]
            # data2 = getData(query2,cursor)[1]

          
            d1 = {}
            d2 = {}
            if data:
                if data[0][0]!=None:
                   
                    d1 = data[0][0]

            # ##print(data2[0][0])
            # if data2:
            #     if data2[0][0]!=None:
                  
            #         d2 = data2[0][0]

            
            # #print(d1)
            
            return Response({'domain': d1,'status':'Success'})

        if inputs['status']=='search_gene':
            final_data = dict()
            
            # query1 = """SELECT array_to_json(array_agg(row_to_json(r))) FROM (SELECT * FROM public.genemaster_table_maize where lower(v4_gene_model_id) ilike lower('""" + str(inputs['content'])+ """') )r"""
            
            if inputs['type']=='Maize':
                table_name = 'zea_mays_b73_refgen_v4_50'

                query1 = """SELECT array_to_json(array_agg(row_to_json(r))) FROM (SELECT * FROM public."""+table_name+""" where lower(gene_id) ilike lower('""" + str(inputs['content'])+ """') )r"""

            if inputs['type']=='Arabidopsis':
                table_name ='arabidopsis_refseq'
                query1 = """SELECT array_to_json(array_agg(row_to_json(r))) FROM (SELECT * FROM public."""+table_name+""" where lower(gene_stable_id) ilike lower('""" + str(inputs['content'])+ """') )r"""

            # query2 = """select string_agg(t1.seq,E'\n') from (
			# 		SELECT concat(E'> Len:',LENGTH(seq),E'\n',description,E'\n',seq) as "seq" FROM public.zea_mays_b73_refgen_v4_cdna_all_fa
            #         where id ilike '%""" + str(inputs['content'])+ """%' ) t1"""
            data = getData(query1,cursor)[1]
            # data2 = getData(query2,cursor)[1]

          
            d1 = {}
            d2 = {}
            if data:
                if data[0][0]!=None:
                   
                    d1 = data[0][0]

            # ##print(data2[0][0])
            # if data2:
            #     if data2[0][0]!=None:
                  
            #         d2 = data2[0][0]

            
            # #print(d1)
            
            return Response({'genedata': d1,'status':'Success'})

        if inputs['status']=='search_mrna':
            final_data = dict()
            
            # query1 = """SELECT array_to_json(array_agg(row_to_json(r))) FROM (SELECT * FROM public.genemaster_table_maize where lower(v4_gene_model_id) ilike lower('""" + str(inputs['content'])+ """') )r"""

            # table_name1 ='arabidopsis_refseq'
            

            query1 = """SELECT array_to_json(array_agg(row_to_json(r))) FROM (SELECT * FROM public.agpv4_5_refseq
                    where gene_stable_id in (
                    SELECT distinct v4_gene_model_id FROM public.evidence_table_maize
                    where lower(v5_gene_model_id) ilike lower('""" + str(inputs['content'])+ """') or 
                    lower(v3genemodelid) ilike lower('""" + str(inputs['content'])+ """') or 
                    lower(v4_gene_model_id) ilike lower('""" + str(inputs['content'])+ """')) )r"""

            
            query2 = """select string_agg(t1.seq,E'\n') from (
					SELECT concat(E'> Len:',LENGTH(seq),E'\n',description,E'\n',seq) as "seq" FROM public.zea_mays_b73_refgen_v4_cdna_all_fa
                    where id ilike '%""" + str(inputs['content'])+ """%' ) t1"""

            if inputs['type']=='Arabidopsis':
                query1 = """SELECT array_to_json(array_agg(row_to_json(r))) FROM (SELECT * FROM public.arabidopsis_refseq
                        where gene_stable_id in (
                        SELECT distinct geneid FROM public.arabidopsis_evidencemasterfile
                        where lower(geneid) ilike lower('""" + str(inputs['content'])+ """') ) )r"""

                
                query2 = """select string_agg(t1.seq,E'\n') from (
                        SELECT concat(E'> Len:',LENGTH(seq),E'\n',description,E'\n',seq) as "seq" FROM public.arabidopsis_thaliana_tair10_cdna_all_fa
                        where id ilike '%""" + str(inputs['content'])+ """%' ) t1"""
            
            print(query1)
            print(query2)
            data = getData(query1,cursor)[1]
            data2 = getData(query2,cursor)[1]

          
            d1 = {}
            d2 = {}
            if data:
                if data[0][0]!=None:
                    if data[0][0][0]!=None:
                        d1 = data[0][0][0]

            # ##print(data2[0][0])
            if data2:
                if data2[0][0]!=None:
                  
                    d2 = data2[0][0]

            

            
            return Response({'refseqdata': d1,'cdna':d2,'status':'Success'})

        if inputs['status']=='search':
           
            final_data = dict()
           
            # query = """SELECT array_to_json(array_agg(row_to_json(r))) FROM (SELECT * FROM public.evidence_masterfile_dummy
            # where lower(v5_gene_model_id) ilike lower('""" + str(inputs['content'])+ """') or 
            # lower(v3_gene_model_id) ilike lower('""" + str(inputs['content'])+ """') or 
            # lower(v4_gene_model_id) ilike lower('""" + str(inputs['content'])+ """')  )r"""
            
            if inputs['type']=='Maize':
                table_name1 = 'genemaster_table_maize'
                table_name2 = 'evidence_table_maize'

                query = """SELECT array_to_json(array_agg(row_to_json(r))) FROM (SELECT * FROM public."""+table_name1+""" where lower(v4_gene_model_id) ilike lower('""" + str(inputs['content'])+ """') )r"""
                
                query2 = """SELECT array_to_json(array_agg(row_to_json(r))) FROM (SELECT * FROM public."""+table_name2+""" where lower(v5_gene_model_id) ilike lower('""" + str(inputs['content'])+ """') or 
                lower(v3genemodelid) ilike lower('""" + str(inputs['content'])+ """') or 
                lower(v4_gene_model_id) ilike lower('""" + str(inputs['content'])+ """'))r"""

            # query2 = """SELECT distinct upper(gene_symbol) as "gene_symbol", upper(full_name) as "full_name" FROM public.evidence_masterfile_dummy
            # where lower(v5_gene_model_id) ilike lower('""" + str(inputs['content'])+ """') or 
            # lower(v3_gene_model_id) ilike lower('""" + str(inputs['content'])+ """') or 
            # lower(v4_gene_model_id) ilike lower('""" + str(inputs['content'])+ """')"""

            
            if inputs['type']=='Arabidopsis':
                table_name1 ='arabidopsis_genemasterfile2'
                table_name2 ='arabidopsis_evidencemasterfile'

                query = """SELECT array_to_json(array_agg(row_to_json(r))) FROM (SELECT * FROM public."""+table_name1+""" where lower(geneid) ilike lower('""" + str(inputs['content'])+ """') limit 1)r"""
                
                query2 = """SELECT array_to_json(array_agg(row_to_json(r))) FROM (SELECT * FROM public."""+table_name2+""" where lower(geneid) ilike lower('""" + str(inputs['content'])+ """') )r"""

            
            

            ##print(query2)
            data = getData(query,cursor)[1]

            data2 = getData(query2,cursor)[1]

            # gene_l = getData(query2,cursor)[1]
            # #print("data",data)

            d1 = []
            d2 = []
            if data:
                if data[0][0]!=None:
                    if data[0][0][0]!=None:
                        d1 = data[0][0][0]

            if data2:
                if data2[0][0]!=None:
                  
                    d2 = data2[0][0]
            
            # print(d1)
            # print("-----------------------")
            # print(d2)
            

            
            return Response({'data': d1,'evidence':d2,'status':'Success'})
            

            

  



        if inputs['status']=='cytoscape_without_stress':
            g = nx.Graph()

            a = 20
            b = 30
            r = 300

            #The lower this value the higher quality the circle is with more points generated
            stepSize = 1

            # species=''
            # stress=''
            gene=("AT1G56242",
                "LNCRNA",
                "AT3G09922"
               )



            final_data = dict()

            # table_name1 ='arabidopsismasterfile_network'
            # if inputs['type']=='Maize':
            #     table_name1 = 'maize_network_master'
            table_name1 = 'maize_network_master'
           
            query = """select * from public."""+table_name1+""" where source in """ + str(inputs['gene'])+ """ """
           
            ###print(query)
            data = getData(query,cursor)[1]

            # gene_l = getData(query2,cursor)[1]
            # #print(data)

            if len(data)==0:
                return Response({'data':{},'properties':{'nodes':'--','avg_path_len':'--','connected':'--', 'edges':'--','avg_diam':'--','g_cluster':'--', 'avg_clust_coff':'--','avg_deg':'--'},'status':'Successful'})

            t=0
            nodes =[]
            edges=[]
            # ###print(gene_l)
            # for j in gene_l:
                
 
            gene_l=[]
            for i in data:
                for j in [0,1]: 
                    if i[j] not in gene_l:
                        ###print(i[j])
                        gene_l.append(i[j])
                        d1= dict()

                        d1['id']=i[j]
                        d1['label']=i[j]

                        d2= dict()
                        x= 100
                        
                        d2['position']={ 'x': r * math.cos(t) + a, 'y':  r * math.sin(t) + b } 
                        d2['data']=d1
                        t += stepSize
                        nodes.append(d2)
                        


                d3=dict()
                g.add_edge(i[0], i[1])
                d3['source']=i[0]
                d3['target']=i[1]
                d3['label']=i[2]
                
                


                edges.append({'data':d3})

                

              
                
               
            final_data['nodes']=nodes
            final_data['edges']=edges
            ###print(final_data)

            G = g#nx.from_numpy_matrix(adj)
            # # G.remove_nodes_from(nx.isolates(G))
            # ###print('num of nodes: {}'.format(G.number_of_nodes()))
            # ###print('num of edges: {}'.format(G.number_of_edges()))
            # G_deg = nx.degree_histogram(G)
            # G_deg_sum = [a * b for a, b in zip(G_deg, range(0, len(G_deg)))]
            # ###print('average degree: {}'.format(sum(G_deg_sum) / G.number_of_nodes()))
            G_deg = nx.degree_histogram(G)
            ##print(G_deg)
            G_deg_sum = [a * b for a, b in zip(G_deg, range(0, len(G_deg)))]
            ###print('average degree: {}'.format(sum(G_deg_sum) / G.number_of_nodes()))
            avg_diam='--'
            avg_path_len ='--'
            if nx.is_connected(G):
                ###print('connected:',nx.is_connected(G))
                # ###print('average path length: {}'.format(nx.average_shortest_path_length(G)))
                # ###print('average diameter: {}'.format(nx.diameter(G)))
                avg_path_len= nx.average_shortest_path_length(G)
                avg_diam = nx.diameter(G)
            G_cluster = sorted(list(nx.clustering(G).values()))
            # ###print('average clustering coefficient: {}'.format(sum(G_cluster) / len(G_cluster)))
            # cycle_len = []
            # cycle_all = nx.cycle_basis(G, 0)
            # for item in cycle_all:
            #     cycle_len.append(len(item))
            # ###print('cycles', cycle_len)
            # ###print('cycle count', len(cycle_len))
            betweenness = nx.betweenness_centrality(G)
            closeness_centrality = nx.closeness_centrality(G)
            properties= {'closeness_centrality':closeness_centrality,'betweenness':betweenness,'nodes':G.number_of_nodes(),'avg_path_len':avg_path_len,'connected':str(nx.is_connected(G)),'avg_deg':sum(G_deg_sum) / G.number_of_nodes(), 'edges':G.number_of_edges(),'avg_diam':avg_diam, 'avg_clust_coff':sum(G_cluster) / len(G_cluster)}
            #print("Done",properties)
            return Response({'data':final_data,'properties':properties,'status':'Successful'})
       
        if inputs['status']=='cytoscape':
            g = nx.Graph()

            a = 20
            b = 30
            r = 300

            #The lower this value the higher quality the circle is with more points generated
            stepSize = 1

            # species=''
            # stress=''
            gene=("AT1G56242",
                "LNCRNA",
                "AT3G09922"
               )


            # if inputs['species']=='Arabidopsis':
            #     file_start = 'arab_'
            # else:
            file_start = inputs['species'].lower() + "_"
            
            # if inputs['stress']=='Water Logging':
            #     file_end = 'wl'
            # else:
            file_end = inputs['stress'].lower()


            file_name = file_start + file_end

            final_data = dict()
           
            query = """select * from """ +file_name+""" where source in """ + str(inputs['gene'])+ """ """
            query2 = """select distinct columns from (
                select source as columns from arab_cold where source in """ + str(gene)+ """ 
                union
                select target from arab_cold where source in """ + str(gene)+ """ ) x"""
            ###print(query)
            data = getData(query,cursor)[1]

            # gene_l = getData(query2,cursor)[1]
            ###print(data)

            if len(data)==0:
                return Response({'data':{},'properties':{'nodes':'--','avg_path_len':'--','connected':'--', 'edges':'--','avg_diam':'--','g_cluster':'--', 'avg_clust_coff':'--','avg_deg':'--'},'status':'Successful'})

            t=0
            nodes =[]
            edges=[]
            # ###print(gene_l)
            # for j in gene_l:
                
 
            gene_l=[]
            for i in data:
                for j in [0,1]: 
                    if i[j] not in gene_l:
                        ###print(i[j])
                        gene_l.append(i[j])
                        d1= dict()

                        d1['id']=i[j]
                        d1['label']=i[j]

                        d2= dict()
                        x= 100
                        
                        d2['position']={ 'x': r * math.cos(t) + a, 'y':  r * math.sin(t) + b } 
                        d2['data']=d1
                        t += stepSize
                        nodes.append(d2)
                        


                d3=dict()
                g.add_edge(i[0], i[1])
                d3['source']=i[0]
                d3['target']=i[1]
                d3['label']=i[2]
                
                


                edges.append({'data':d3})

                

              
                
               
            final_data['nodes']=nodes
            final_data['edges']=edges
            ###print(final_data)

            G = g#nx.from_numpy_matrix(adj)
            # # G.remove_nodes_from(nx.isolates(G))
            # ###print('num of nodes: {}'.format(G.number_of_nodes()))
            # ###print('num of edges: {}'.format(G.number_of_edges()))
            # G_deg = nx.degree_histogram(G)
            # G_deg_sum = [a * b for a, b in zip(G_deg, range(0, len(G_deg)))]
            # ###print('average degree: {}'.format(sum(G_deg_sum) / G.number_of_nodes()))
            G_deg = nx.degree_histogram(G)
            ##print(G_deg)
            G_deg_sum = [a * b for a, b in zip(G_deg, range(0, len(G_deg)))]
            ###print('average degree: {}'.format(sum(G_deg_sum) / G.number_of_nodes()))
            avg_diam='--'
            avg_path_len ='--'
            if nx.is_connected(G):
                ###print('connected:',nx.is_connected(G))
                # ###print('average path length: {}'.format(nx.average_shortest_path_length(G)))
                # ###print('average diameter: {}'.format(nx.diameter(G)))
                avg_path_len= nx.average_shortest_path_length(G)
                avg_diam = nx.diameter(G)
            G_cluster = sorted(list(nx.clustering(G).values()))
            # ###print('average clustering coefficient: {}'.format(sum(G_cluster) / len(G_cluster)))
            # cycle_len = []
            # cycle_all = nx.cycle_basis(G, 0)
            # for item in cycle_all:
            #     cycle_len.append(len(item))
            # ###print('cycles', cycle_len)
            # ###print('cycle count', len(cycle_len))
            betweenness = nx.betweenness_centrality(G)
            closeness_centrality = nx.closeness_centrality(G)
            properties= {'closeness_centrality':closeness_centrality,'betweenness':betweenness,'nodes':G.number_of_nodes(),'avg_path_len':avg_path_len,'connected':str(nx.is_connected(G)),'avg_deg':sum(G_deg_sum) / G.number_of_nodes(), 'edges':G.number_of_edges(),'avg_diam':avg_diam, 'avg_clust_coff':sum(G_cluster) / len(G_cluster)}

            return Response({'data':final_data,'properties':properties,'status':'Successful'})