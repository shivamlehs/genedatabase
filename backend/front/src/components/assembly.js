export default {
    name: 'GRCh38',
    sequence: {
        type: 'ReferenceSequenceTrack',
        trackId: 'GRCh38-ReferenceSequenceTrack',
        adapter: {
            type: 'BgzipFastaAdapter',
            fastaLocation: {
                uri:
                    'https://s3.amazonaws.com/jbrowse.org/genomes/GRCh38/fasta/GRCh38.fa.gz',
            },
            faiLocation: {
                uri:
                    'https://s3.amazonaws.com/jbrowse.org/genomes/GRCh38/fasta/GRCh38.fa.gz.fai',
            },
            gziLocation: {
                uri:
                    'https://s3.amazonaws.com/jbrowse.org/genomes/GRCh38/fasta/GRCh38.fa.gz.gzi',
            },
        },
    },
    aliases: ['hg38'],
    refNameAliases: {
        adapter: {
            type: 'RefNameAliasAdapter',
            location: {
                uri: 'GRCh38.aliases.txt',
            },
        },
    },
}