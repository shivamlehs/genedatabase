import React, { Component } from "react"
// import Modal from "./components/Modal"; 
import axios from "axios";
import { Link } from "react-router-dom";
import Sidebar from "./Sidebar";

var delayb4scroll = 2000 //Specify initial delay before marquee starts to scroll on page (2000=2 seconds)
var marqueespeed = 1 //Specify marquee scroll speed (larger is faster 1-10)
var pauseit = 1 //Pause marquee onMousever (0=no. 1=yes)?

////NO NEED TO EDIT BELOW THIS LINE////////////

function isEmpty(obj) {
    return Object.keys(obj).length === 0;
}

// var copyspeed = marqueespeed
// var pausespeed = (pauseit == 0) ? copyspeed : 0
// var actualheight = ''
// var cross_marquee = { style: { top: '8px' } }
// // var cross_marquee = ''
// var marqueeheight = '3'
// function scrollmarquee() {
//   if (parseInt(cross_marquee.style.top) > (actualheight * (-1) + 8)) //if scroller hasn't reached the end of its height
//     cross_marquee.style.top = parseInt(cross_marquee.style.top) - copyspeed + "px" //move scroller upwards
//   else //else, reset to original position
//     cross_marquee.style.top = parseInt(marqueeheight) + 8 + "px"
// }

// function initializemarquee() {
//   cross_marquee = document.getElementById("vmarquee")
//   cross_marquee.style.top = 0
//   marqueeheight = document.getElementById("marqueecontainer").offsetHeight
//   actualheight = cross_marquee.offsetHeight //height of marquee content (much of which is hidden from view)
//   if (window.opera || navigator.userAgent.indexOf("Netscape/7") != -1) { //if Opera or Netscape 7x, add scrollbars to scroll and exit
//     cross_marquee.style.height = marqueeheight + "px"
//     cross_marquee.style.overflow = "scroll"
//     return
//   }
//   setTimeout('lefttime=setInterval("scrollmarquee()",30)', delayb4scroll)
// }

// if (window.addEventListener)
//   window.addEventListener("load", initializemarquee, false)
// else if (window.attachEvent)
//   window.attachEvent("onload", initializemarquee)
// else if (document.getElementById)
//   window.onload = initializemarquee


// function checkInput() {
//   var len = document.getElementById('search-text').value.length;
//   if (len < 1) {
//     alert('Please enter a search value');
//     return false;
//   }
// }



// on click of one of tabs
function displayPage() {
    var current = this.parentNode.getAttribute("data-current");
    //remove class of activetabheader and hide old contents
    document.getElementById("tabHeader_" + current).removeAttribute("class");
    document.getElementById("tabpage_" + current).style.display = "none";

    var ident = this.id.split("_")[1];
    //add class of activetabheader to new active tab and show contents
    this.setAttribute("class", "tabActiveHeader");
    document.getElementById("tabpage_" + ident).style.display = "block";
    this.parentNode.setAttribute("data-current", ident);
}


class SearchResult extends Component {
    state = {
        viewCompleted: false,
        activeItem: {
            title: "",
            description: "",
            completed: false,


        },
        data: [],
        type: 'Maize',
        generalinfo: { 'gene_symbol': '', 'geneid': '', 'name': '' },
        mrnadata: {},
        mrnaseq: {},
        evidence: [],
        todoList: [],
        genedata: [],
        refseqdata: {},
        cdsdata: {},
        protein: { 'uniprotdata': [], 'protein': '', 'status': 'Success' },
        cdna: 'No',
        content: '',
        domain: [],
        ontology: [],
        species: 'Maize'
    };

    async componentDidMount() {
        try {
            // const res = await fetch('http://13.234.5.219:8000/api/todos/');
            // const todoList = await res.json();
            // this.setState({
            //   todoList
            // });
            // var filter_data = { 'status': 'search', 'type': this.state.type, 'content': this.state.content };
            // axios.post('http://13.234.5.219:8000/api/esanjeevni', filter_data).then(resp => {
            //   console.log('Data', resp)

            //   this.setState({ elements: resp.data.data, properties: resp.data.properties })
            // });
        } catch (e) {
            console.log(e);
        }
        // window.addEventListener('load', this.handleLoad);

    }


    toggle = () => {
        this.setState({ modal: !this.state.modal });
    };


    handleChange = (event) => {
        // console.log("Event", event.target.value)

        this.setState({ type: event.target.value });
    }
    handleChange2 = (event) => { this.setState({ content: event.target.value }); }
    // handleChange3 = (event) => { this.setState({ gene: event.target.value }); }

    getArticlesFromApi = async () => {
        try {
            let response = await fetch(
                'https://examples.com/data.json'
            );
            let json = await response.json();
            return json.movies;
        } catch (error) {
            console.error(error);
        }
    };

    handleSubmit = (event) => {
        alert('A name was submitted: ' + this.state.species);
        event.preventDefault();

        try {

            var filter_data = { 'status': 'search', 'type': this.state.type, 'content': this.state.content };
            axios.post('http://13.234.5.219:8000/api/esanjeevni', filter_data).then(resp => {
                // console.log('Data', resp)

                this.setState({ data: resp.data.data, evidence: resp.data.evidence })
            });


        } catch (e) {
            console.log(e);
        }

        try {

            var filter_data = { 'status': 'search_gene', 'type': this.state.type, 'content': this.state.content };
            axios.post('http://13.234.5.219:8000/api/esanjeevni', filter_data).then(resp => {


                this.setState({ genedata: resp.data.genedata })
                console.log('Data2', this.state.genedata)
            });


        } catch (e) {
            console.log(e);
        }

        try {

            var filter_data = { 'status': 'search_cds', 'type': this.state.type, 'content': this.state.content };
            axios.post('http://13.234.5.219:8000/api/esanjeevni', filter_data).then(resp => {


                this.setState({ cdsdata: resp.data.cdsdata })
                // console.log('Data2', this.state.genedata)
            });


        } catch (e) {
            console.log(e);
        }

        try {

            var filter_data = { 'status': 'search_protein', 'type': this.state.type, 'content': this.state.content };
            axios.post('http://13.234.5.219:8000/api/esanjeevni', filter_data).then(resp => {


                this.setState({ protein: resp.data })
                // console.log('Data2', this.state.genedata)
            });


        } catch (e) {
            console.log(e);
        }



        try {

            var filter_data = { 'status': 'search_mrna', 'type': this.state.type, 'content': this.state.content };
            axios.post('http://13.234.5.219:8000/api/esanjeevni', filter_data).then(resp => {


                this.setState({ refseqdata: resp.data.refseqdata, cdna: resp.data.cdna })
                // console.log('Data2', this.state.cdna)
            });


        } catch (e) {
            console.log(e);
        }


        try {

            var filter_data = { 'status': 'search_domain', 'type': this.state.type, 'content': this.state.content };
            axios.post('http://13.234.5.219:8000/api/esanjeevni', filter_data).then(resp => {


                this.setState({ domain: resp.data.domain })
                // console.log('Data2', this.state.cdna)
            });


        } catch (e) {
            console.log(e);
        }

        try {

            var filter_data = { 'status': 'search_ontology', 'type': this.state.type, 'content': this.state.content };
            axios.post('http://13.234.5.219:8000/api/esanjeevni', filter_data).then(resp => {


                this.setState({ ontology: resp.data.ontology })
                // console.log('Data2', this.state.cdna)
            });


        } catch (e) {
            console.log(e);
        }
    }

    createItem = () => {
        const item = { title: "", description: "", completed: false };
        this.setState({ activeItem: item, modal: !this.state.modal });
    };

    displayCompleted = status => {
        if (status) {
            return this.setState({ viewCompleted: true });
        }
        return this.setState({ viewCompleted: false });
    };
    handleSubmit = (event) => { };
    renderTabList = () => {
        return (
            <div className="my-5 tab-list">
                <button
                    onClick={() => this.displayCompleted(true)}
                    className={this.state.viewCompleted ? "active" : ""}
                >
                    Complete
                </button>
                <button
                    onClick={() => this.displayCompleted(false)}
                    className={this.state.viewCompleted ? "" : "active"}
                >
                    Incomplete
                </button>
            </div>
        );
    };

    renderItems = () => {
        const { viewCompleted } = this.state;
        const newItems = this.state.todoList.filter(
            item => item.completed === viewCompleted
        );
        return newItems.map(item => (
            <li
                key={item.id}
                className="list-group-item d-flex justify-content-between align-items-center"
            >
                <span
                    className={`todo-title mr-2 ${this.state.viewCompleted ? "completed-todo" : ""
                        }`}
                    title={item.description}
                >
                    {item.title}
                </span>
            </li>
        ));
    };

    render() {
        var chunkedPartsArray = this.state.evidence;
        var gene_data = this.state.genedata;
        var protein_data = this.state.protein.uniprotdata;
        var domain_data = this.state.domain;
        var ontology_data = this.state.ontology;
        return (
            <div id="wrapper">

                <div id="content">
                    <div class="recent">
                        {/* <!-- begin post --> */}
                        <div class="single">
                            <h2>Submit Data</h2><br /><br />
                            <form onSubmit={this.handleSubmit} >
                                <b>Name: </b>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <input type="text" name="name" style={{ height: '30px', width: '200px' }} /><br /><br />
                                <b>Organization: </b> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <input type="text" name="organization" style={{ height: '30px', width: '200px' }} /><br /><br />
                                <b>Email: </b>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <input type="text" name="email" style={{ height: '30px', width: '200px' }} /><br /><br />
                                <b>Pubmed ID: </b>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <input type="text" name="pubmedid" style={{ height: '30px', width: '200px' }} /><br /><br />
                                <b>Publication Year: </b>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <input type="text" name="year" style={{ height: '30px', width: '200px' }} /><br /><br />
                                <b>Gene Name: </b> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <input type="text" name="genename" style={{ height: '30px', width: '200px' }} /><br /><br />
                                <b>Comments: </b><br /><br /><textarea name="comments" cols="40" rows="6" style={{ width: '516px', height: '164px' }} ></textarea><br /><br />
                                <input type="submit" style={{ height: '30px', width: '137px' }} />
                            </form>
                        </div>
                        {/* <!-- end post --> */}
                    </div>


                </div >

                <Sidebar />

            </div >


        )
    }
}

export default SearchResult;
// import logo from './logo.svg';
// import './App.css';

// function App() {
//   return (
    // <div className="App">
    //   <header className="App-header">
    //     <img src={logo} className="App-logo" alt="logo" />
    //     <p>
    //       Edit <code>src/App.js</code> and save to reload.
    //     </p>
    //     <a
    //       className="App-link"
    //       href="https://reactjs.org"
    //       target="_blank"
    //       rel="noopener noreferrer"
    //     >
    //       Learn React
    //     </a>
    //   </header>
    // </div>
//   );
// }

// export default App;
